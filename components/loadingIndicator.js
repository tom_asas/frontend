import React from 'react';
import { View, ActivityIndicator } from 'react-native';

export function LoadingIndicator() {
    return (
        <View style={{ justifyContent: 'center', height: '100%' }}>
            <ActivityIndicator size="large" color="grey" />
        </View>
    );
}

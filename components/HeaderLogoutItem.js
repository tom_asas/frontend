import React, { useEffect, useState } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../external/firebaseAuth';

export function HeaderLogoutItem({ navigation }) {
    const [signed, setIsSigned] = useState(false);

    const handleSignOut = () => {
        auth.signOut()
            .then(() => {
                navigation.navigate('Login');
            })
            .catch();
    };

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setIsSigned(true);
            } else {
                setIsSigned(false);
            }
        });
    }, []);

    if (!signed) return null;
    return (
        <TouchableOpacity
            style={{
                height: 50,
                width: 70,
                backgroundColor: '#FDCB00',
                justifyContent: 'center',
                borderRadius: 15,
                borderWidth: 1,
                marginRight: 10,
            }}
            onPress={handleSignOut}
        >
            <Text
                style={{
                    alignSelf: 'center',
                    fontWeight: 'bold',
                    color: '#14171A',
                }}
            >
                Logout
            </Text>
        </TouchableOpacity>
    );
}

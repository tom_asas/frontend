import
{
    collection,
    getDoc,
    doc,
    query,
    getDocs,
    setDoc,
}
    from 'firebase/firestore';
import { db } from '../external/firebaseAuth';

async function UpsertRecord(item) {
    const document = await GetRecord(item.email);

    if (document !== null && document !== undefined) {
        await setDoc(doc(db, 'quizz', item.email), {
            email: item.email,
            score: item.userScore + document.score,
        });
    } else {
        await setDoc(doc(db, 'quizz', item.email), {
            email: item.email,
            score: item.userScore,
        });
    }
}

async function GetRecords() {
    const q = query(collection(db, 'quizz'));
    const querySnapshot = await getDocs(q);

    return querySnapshot.docs.map((record) => record.data());
}

async function GetRecord(email) {
    const docRef = doc(db, 'quizz', email);
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
        return docSnap.data();
    }
    return null;
}

export const FireBaseService = {
    UpsertRecord,
    GetRecords,
    GetRecord,
};

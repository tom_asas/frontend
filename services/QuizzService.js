import base64 from 'react-native-base64';

function getAllAnswers(item) {
    let answers = item.incorrect_answers;
    const corAnswers = item.correct_answer;
    answers.push(corAnswers);
    answers = shuffleArray(answers);
    return answers;
}

function shuffleArray(array) {
    let currentIndex = array.length;
    let randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex],
            array[currentIndex],
        ];
    }
    return array;
}

function calculateResult(answers, correctAnswers) {
    let score = 0;

    for (let i = 0; i < correctAnswers.length; i++) {
        const correctAnwer = decodeText(correctAnswers[i].correct_answer);
        if (answers[i] !== undefined && correctAnwer === answers[i]) {
            score++;
        }
    }

    return score;
}

function decodeText(input) {
    return base64.decode(input);
}

async function getQuiz(url) {
    const temp = await fetch(
        url,
        {
            method: 'GET',
        },
    ).then((data) => data.json());

    return temp.results;
}

export const QuizzService = {
    getAllAnswers,
    calculateResult,
    getQuiz,
    decodeText,
};

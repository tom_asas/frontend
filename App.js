import React, { useState, useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { Questions } from './screens/QuestionScreen';
import { Home } from './screens/HomeScreen';
import { QuizzResults } from './screens/ResultsScreen';
import { LoginScreen } from './screens/LoginScreen';
import { HeaderLogoutItem } from './components/HeaderLogoutItem';
import { Categories } from './screens/Categories';
import { Profile } from './screens/ProfileScreen';
import { auth } from './external/firebaseAuth';

const Drawer = createDrawerNavigator();

const navigationHeaderStyles = {
    headerStyle: { backgroundColor: 'rgba(52, 52, 52, 0.8)' },
    headerTitleStyle: { color: 'white' },
    headerTintColor: 'white',
};

export default function App() {
    const [isLogged, setIsLogged] = useState(null);

    useEffect(() => {
        const userAuth = auth.currentUser;

        if (userAuth === null) {
            setIsLogged('none');
        } else {
            setIsLogged('flex');
        }
    }, []);

    return (
        <NavigationContainer>
            <Drawer.Navigator
                initialRouteName="Login"
                screenOptions={navigationHeaderStyles}
            >
                <Drawer.Screen
                    name="Quizz"
                    component={Questions}
                    options={{
                        headerShown: false,
                        swipeEnabled: false,
                        drawerItemStyle: { display: 'none' },
                    }}
                />
                <Drawer.Screen
                    name="Logout"
                    component={HeaderLogoutItem}
                    options={{
                        headerShown: false,
                        swipeEnabled: false,
                        drawerItemStyle: { display: 'none' },
                    }}
                />
                <Drawer.Screen
                    name="Home"
                    component={Home}
                />
                <Drawer.Screen
                    name="Results"
                    component={QuizzResults}
                    options={{
                        drawerItemStyle: { display: 'none' },
                    }}
                />
                <Drawer.Screen
                    options={{
                        headerShown: false,
                        swipeEnabled: false,
                        drawerItemStyle: { display: 'none' },
                    }}
                    name="Login"
                    component={LoginScreen}
                />
                <Drawer.Screen
                    name="Categories"
                    component={Categories}
                />
                <Drawer.Screen
                    name="Profile"
                    component={Profile}
                    options={{
                        drawerItemStyle: { display: isLogged },
                    }}
                />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}

// Import the functions you need from the SDKs you need
// import * as firebase from 'firebase';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyCSB9a4TV4Xyindg9gPAwKZqu33oI-CQ6k',
    authDomain: 'quizzapp-345002.firebaseapp.com',
    projectId: 'quizzapp-345002',
    storageBucket: 'quizzapp-345002.appspot.com',
    messagingSenderId: '299631657655',
    appId: '1:299631657655:web:656ffbfe6a84da36c3c332',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const db = getFirestore(app);

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        width: '80%',
    },
    input: {
        backgroundColor: 'white',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 5,
    },
    buttonContainer: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 80,
    },
    button: {
        backgroundColor: '#FDCB00',
        width: '120%',
        padding: 20,
        borderRadius: 15,
        marginBottom: 30,
        alignItems: 'center',
    },
    buttonOutline: {
        backgroundColor: 'white',
        marginTop: 5,
        borderColor: '#1DA1F2',
        borderWidth: 2,
    },
    buttonRetryText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
    },
    buttonRegisterText: {
        color: '#14171A',
        fontWeight: 'bold',
        fontSize: 16,
    },
    buttonGuest: {
        backgroundColor: '#657786',
        width: '100%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
    },
    headerText: {
        color: '#14171A',
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default styles;

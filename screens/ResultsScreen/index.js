import React from 'react';
import { View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';

export function QuizzResults({ route, navigation }) {
    const { numberOfQuestions, correctAnswers } = route.params;

    function getComment() {
        let comment = 'Yeet';
        comms1 = ['You might need to deepen your knowledge on the topic!', 'Welp, better luck next time!', 'You might need to do some more reading :)'];
        comms2 = ['You are doing great, but dont stop just yet!', 'This time was not that bad, but dont get too comfortable ;)', 'Keep at it, you are getting there!'];
        comms3 = ['Excellent work! Your knowledge is pretty strong on the topic!', 'Awesome! You know quite a lot', 'We have a winner here!'];
        if (correctAnswers <= 4) {
            comment = comms1[Math.floor(Math.random() * comms1.length)];
        } else if (correctAnswers > 4 && correctAnswers <= 7) {
            comment = comms2[Math.floor(Math.random() * comms1.length)];
        } else if (correctAnswers > 7) {
            comment = comms3[Math.floor(Math.random() * comms1.length)];
        }
        return comment;
    }

    function getPointEffect() {
        let points = '';
        const pts = correctAnswers * 10;

        if (pts > 0) {
            points = `+ ${pts} pts`;
        }
        if (pts === 100) {
            points += ' (Wow!)';
        }
        return points;
    }

    return (
        <View style={{ height: '100%', justifyContent: 'center' }}>
            <View style={{ borderWidth: 0, borderColor: 'black' }}>
                <Text style={{ fontSize: 30, alignSelf: 'center', fontWeight: 'bold' }}>
                    Recap:
                </Text>
            </View>
            <View style={{ marginTop: 20 }}>
                <Text
                    style={{
                        fontSize: 25,
                        fontWeight: 'bold',
                        alignSelf: 'center',
                    }}
                >
                    {correctAnswers === undefined
                        ? 'Hm, did your really take the quizz?'
                        : `${correctAnswers} / ${numberOfQuestions}`}
                </Text>
            </View>
            <View style={{ marginTop: 5 }}>
                <Text style={{ fontSize: 20, alignSelf: 'flex-end', color: 'green', marginRight: 40, fontWeight: 'bold'}}>
                    { getPointEffect()}
                </Text>
                <View style={{ marginTop: 40, padding: 20 }}>
                    <Text style={{ fontSize: 25, alignSelf: 'center', color: 'orange', fontWeight: 'bold' }}>
                        {getComment()}
                    </Text>
                </View>
            </View>
            <View style={styles.container} behavior="padding">
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => navigation.navigate('Categories')}
                    >
                        <Text
                            style={styles.buttonRetryText}
                        >
                            Try again
                        </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                        style={styles.button}
                        // onPress={() => navigation.navigate('Quizz')}  -- some Menu screen
                    >
                        <Text style={styles.buttonRetryText}>
                            Test out knowledge in other areas
                        </Text>
                    </TouchableOpacity> */}
                </View>
            </View>
        </View>
    );
}

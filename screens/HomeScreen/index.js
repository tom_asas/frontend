import React from 'react';
import { ImageBackground, View, Text } from 'react-native';
import styles from './styles';

export function Home({ navigation }) {
    return (
        <ImageBackground source={require('../../Images/home-background.jpg')} View style={styles.container}>
            <View style={styles.innerContainer}>
                <Text style={styles.infoText}>
                Welcome to Quizz! Here you can test your knowledge
                 in various topics by answering Quiz questions.
                 Select a new Quiz by pressing the button below.
                </Text>
                <Text
                    style={styles.selectButton}
                    onPress={() => navigation.navigate('Categories')}
                >
                Select Quiz
                </Text>
            </View>
        </ImageBackground>
    );
}

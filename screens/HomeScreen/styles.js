import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    innerContainer: {
        borderWidth: 1,
        borderColor: '#aa00ff',
        backgroundColor: '#000000cc',
        width: '80%',
        alignItems: 'center',
        paddingHorizontal: 25,
        paddingVertical: 35,
        borderRadius: 35,
    },
    infoText: {
        fontSize: 16,
        textAlign: 'center',
        color: '#ffb400',
    },
    selectButton: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ff6400',
        color: '#00bcd4',
        backgroundColor: '#0000007f',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 35,
        fontSize: 20,
    },
});

export default styles;

import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import styles from './styles';
import { Category, Difficulty, NumCat } from './constants';

export function Categories({ navigation }) {
    const [SelectedCategory, SetSelectedCategory] = useState('General Knowledge');
    const [SelectedDifficulty, SetSelectedDifficulty] = useState('Easy');

    // find an object with selected values,pass on API link to quiz page
    function handlePress() {
        const path = `https://opentdb.com/api.php?amount=10&category=${NumCat[SelectedCategory]}&difficulty=${SelectedDifficulty.toLowerCase()}&encode=base64`;
        navigation.navigate('Quizz', {
            adress: path,
        });
    }
    return (
        <>
            <View style={styles.container}>
                {Category
                &&
                <Picker
                    selectedValue={SelectedCategory}
                    onValueChange={(itemValue) => SetSelectedCategory(itemValue)}
                >
                    {Category.map((answer) => (
                        <Picker.Item
                            key={answer}
                            label={answer}
                            value={answer}
                        />
                    ))}
                </Picker> }
                <Picker
                    selectedValue={SelectedDifficulty}
                    onValueChange={(itemValue) => SetSelectedDifficulty(itemValue)}
                >
                    {Difficulty.map((answer) => (
                        <Picker.Item
                            key={answer}
                            label={answer}
                            value={answer}
                        />
                    ))}
                </Picker>
            </View>
            <View style={styles.buttonView}>
                <TouchableOpacity
                    style={styles.button}
                    // make button pass on to quizz page for now
                    // later make handlePress() work properly
                    onPress={() => handlePress()}
                >
                    <Text style={styles.textSize}>Begin</Text>
                </TouchableOpacity>
            </View>
        </>
    );
}
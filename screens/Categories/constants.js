export const Difficulty = [
    'easy',
    'medium',
    'hard'
];

export const Category = [
    'General Knowledge',
    'History',
    'Sports'
];
export const NumCat = {
    'General Knowledge': 9,
    'History': 23,
    'Sports' : 21
}
// export const Variety = [
//     { 
//       difficulty: 'Easy',
//       category: 'General Knowledge',
//       link: 'https://opentdb.com/api.php?amount=10&category=9&difficulty=easy'
//     },
//     { 
//         difficulty: 'Medium',
//         category: 'General Knowledge',
//         link: 'https://opentdb.com/api.php?amount=10&category=9&difficulty=medium'
//     },
//     { 
//         difficulty: 'Hard',
//         category: 'General Knowledge',
//         link: 'https://opentdb.com/api.php?amount=10&category=9&difficulty=hard'
//     },
//     { 
//         difficulty: 'Easy',
//         category: 'History',
//         link: 'https://opentdb.com/api.php?amount=10&category=23&difficulty=easy'
//     },
//     { 
//         difficulty: 'Medium',
//         category: 'History',
//         link: 'https://opentdb.com/api.php?amount=10&category=23&difficulty=medium'
//     },
//     { 
//         difficulty: 'hard',
//         category: 'History',
//         link: 'https://opentdb.com/api.php?amount=10&category=23&difficulty=hard'
//     },
//     { 
//         difficulty: 'Easy',
//         category: 'Sports',
//         link: 'https://opentdb.com/api.php?amount=10&category=21&difficulty=easy'
//     },
//     { 
//         difficulty: 'Medium',
//         category: 'Sports',
//         link: 'https://opentdb.com/api.php?amount=10&category=21&difficulty=medium'
//     },
//     { 
//         difficulty: 'Hard',
//         category: 'Sports',
//         link: 'https://opentdb.com/api.php?amount=10&category=21&difficulty=hard'
//     },
// ];
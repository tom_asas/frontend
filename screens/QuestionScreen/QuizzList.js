import React from 'react';
import { FlatList, View } from 'react-native';
import { QuizzItem } from './QuizzItem';

export function QuizzList({ items, answers }) {
    return (
        <View>
            <FlatList
                data={items}
                renderItem={(item) => (
                    <QuizzItem
                        item={item}
                        answers={answers}
                    />
                )}
                keyExtractor={(item) => item.question}
                pageNumber={1}
            />
        </View>
    );
}
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        height: '80%',
        width: '100%',
    },
    buttonView: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        height: '20%',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#FDCB00',
        width: '95%',
        height: 75,
        justifyContent: 'center',
        borderRadius: 15,
    },
    textSize: {
        fontSize: 25,
    },
});

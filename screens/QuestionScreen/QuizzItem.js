import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { QuizzService } from '../../services/QuizzService';

export function QuizzItem({ item, answers }) {
    const [selectedAnswer, setSelectedAnswer] = useState(null);
    const [quizzAnswers, setQuizzAnswers] = useState(null);
    const [question, setQuestion] = useState(null);

    useEffect(() => {
        const decodedQuestion = QuizzService.decodeText(item.item.question);
        setQuestion(decodedQuestion);

        const decodedAnswers = QuizzService.getAllAnswers(item.item);

        for (let i = 0; i < decodedAnswers.length; i++) {
            decodedAnswers[i] = QuizzService.decodeText(decodedAnswers[i]);
            // eslint-disable-next-line no-param-reassign
            // eslint-disable-next-line prefer-destructuring
            answers[i] = decodedAnswers[i][0];
        }

        setQuizzAnswers(decodedAnswers);
    }, []);

    function onChange(value) {
        setSelectedAnswer(value);
        if (answers[item.index] !== value) {
            // eslint-disable-next-line no-param-reassign
            answers[item.index] = value;
        }
    }

    return (
        <View>
            <View style={{ marginLeft: 5 }}>
                <Text
                    style={{
                        fontSize: 15,
                        fontWeight: 'bold',
                        textAlign: 'center',
                    }}
                >
                    {question}
                </Text>
                {quizzAnswers && (
                    <Picker
                        selectedValue={selectedAnswer}
                        onValueChange={(itemValue) => onChange(itemValue)}
                    >
                        {quizzAnswers.map((answer) => (
                            <Picker.Item
                                key={answer}
                                label={answer}
                                value={answer}
                            />
                        ))}
                    </Picker>
                )}
            </View>
            <View
                style={{
                    width: '95%',
                    height: 1,
                    backgroundColor: 'grey',
                    marginBottom: 5,
                    alignSelf: 'center',
                }}
            />
        </View>
    );
}

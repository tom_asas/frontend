import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import {
    getAuth,
} from 'firebase/auth';
import { QuizzList } from './QuizzList';
import { QuizzService } from '../../services/QuizzService';
import { FireBaseService } from '../../services/FireBaseService';
import { LoadingIndicator } from '../../components/loadingIndicator';
import { styles } from './style';

export function Questions({ navigation, route }) {
    const [quizz, setQuizz] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [auth, SetAuth] = useState(null);

    const { adress } = route.params;
    const isFocused = useIsFocused();
    const answers = [];

    async function handlePress() {
        setIsLoading(true);
        const score = QuizzService.calculateResult(answers, quizz);

        if (auth !== null) {
            await FireBaseService.UpsertRecord({
                email: auth.email,
                userScore: score * 10,
            });
        }

        navigation.navigate('Results', {
            numberOfQuestions: quizz.length,
            correctAnswers: score,
        });
    }

    useEffect(async () => {
        if (isFocused === true) {
            setIsLoading(true);
            const userAuth = getAuth().currentUser;
            SetAuth(userAuth);

            const response = await QuizzService.getQuiz(adress);
            setQuizz(response);

            setIsLoading(false);
        }
    }, [isFocused]);

    if (isLoading) return <LoadingIndicator />;

    return (
        <>
            <View style={styles.container}>
                {quizz && <QuizzList items={quizz} answers={answers} />}
            </View>
            <View style={styles.buttonView}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={async () => await handlePress()}
                >
                    <Text style={styles.textSize}>Submit</Text>
                </TouchableOpacity>
            </View>
        </>
    );
}

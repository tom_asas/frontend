import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#494949',
        flex: 1,
        alignItems: 'center',
    },
    profileInfoHeader: {
        fontWeight: 'bold',
        fontSize: 30,
        borderBottomWidth: 1,
        borderBottomColor: '#fccb00',
        color: '#fccb00',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 25,
        paddingVertical: 45,
        textAlign: 'center',
    },
    profileInfoSubHeader: {
        color: '#fccb00',
        width: '100%',
        alignItems: 'center',
        fontSize: 30,

        textAlign: 'center',
    },
    profileInfoSubData: {
        color: 'white',
        width: '100%',
        alignItems: 'center',
        fontSize: 24,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    logoutButton: {
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#fccb00',
        color: '#fccb00',
        paddingHorizontal: 25,
        paddingVertical: 15,
        marginTop: 75,
        fontSize: 20,
    },
});

export default styles;

import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../../external/firebaseAuth';
import { FireBaseService } from '../../services/FireBaseService';
import { LoadingIndicator } from '../../components/loadingIndicator';
import styles from './styles';

export function Profile({ navigation }) {
    const [signed, setIsSigned] = useState(false);
    const [score, setScore] = useState(undefined);
    const [isLoading, setIsLoading] = useState(true);

    const isFocused = useIsFocused();

    useEffect(async () => {
        if (isFocused === true) {
            setIsLoading(true);
            const rez = await userScore();

            setScore(rez);
            onAuthStateChanged(auth, (user) => {
                if (user) {
                    setIsSigned(true);
                } else {
                    setIsSigned(false);
                }
            });

            setIsLoading(false);
        }
    }, [isFocused, score]);

    const handleSignOut = () => {
        if (!signed) return null;
        auth.signOut()
            .then(() => {
                navigation.navigate('Login');
            })
            .catch();
    };

    function userEmail() {
        if (signed) {
            return auth.currentUser.email;
        }

        return 'you need to log in to see email';
    }

    async function userScore() {
        if (signed) {
            const email = userEmail();
            const data = await FireBaseService.GetRecord(email);

            console.log(data);

            if (data == null) {
                return 0;
            }

            return data.score;
        }

        return 0;
    }

    if (isLoading) return <LoadingIndicator />;

    return (
        <View style={styles.container}>
            <Text style={styles.profileInfoHeader}>
                Profile Information
            </Text>
            <View style={styles.profileInfoHeader}>
                <Text style={styles.profileInfoSubHeader}>
                    Email
                </Text>
                <Text style={styles.profileInfoSubData}>
                    {userEmail()}
                </Text>
            </View>
            <View style={styles.profileInfoHeader}>
                <Text style={styles.profileInfoSubHeader}>
                    Total Score
                </Text>
                <Text style={styles.profileInfoSubData}>
                    {score === undefined ? 0 : score}
                </Text>
            </View>

            <TouchableOpacity
                style={{
                    height: 50,
                    width: 150,
                    margin: 15,
                    backgroundColor: '#FDCB00',
                    justifyContent: 'center',
                    borderRadius: 15,
                    borderWidth: 1,
                    marginRight: 10,
                }}
                onPress={handleSignOut}
            >
                <Text
                    style={{
                        alignSelf: 'center',
                        fontWeight: 'bold',
                        color: '#14171A',
                    }}
                >
                Logout
                </Text>
            </TouchableOpacity>
        </View>
    );
}

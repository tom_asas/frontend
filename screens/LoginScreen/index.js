import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    KeyboardAvoidingView,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import {
    getAuth,
    createUserWithEmailAndPassword,
    onAuthStateChanged,
    signInWithEmailAndPassword,
} from 'firebase/auth';
import { LoadingIndicator } from '../../components/loadingIndicator';
import styles from './styles';

export function LoginScreen({ navigation }) {
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const auth = getAuth();
    useEffect(() => {
        setIsLoading(true);
        onAuthStateChanged(auth, (user) => {
            if (user) {
                navigation.navigate('Home');
            }
        });
        setIsLoading(false);
    }, []);

    const handleSignUp = () => {
        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredentials) => {
                const { user } = userCredentials;
                console.log('Registered with:', user.email);
            })
            .catch((error) => alert(error.message));
    };

    const handleLogin = () => {
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredentials) => {
                const { user } = userCredentials;
                console.log('Logged in with:', user.email);
            })
            .catch((error) => alert(error.message));
    };

    if (isLoading) return <LoadingIndicator />;
    return (
        <KeyboardAvoidingView
            style={[styles.container, { backgroundColor: '#F5F8FA' }]}
            behavior="padding"
        >
            <View style={styles.headerText}>
                <Text style={{ fontSize: 30 }}>Welcome to Quizz App !</Text>
                <Text style={{ fontSize: 18 }}>
                    Please choose your authentication method
                </Text>
            </View>
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Email"
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                    style={styles.input}
                />
                <TextInput
                    placeholder="Password"
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                    secureTextEntry
                    style={styles.input}
                />
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.button} onPress={handleLogin}>
                    <Text style={styles.buttonLoginText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.button, styles.buttonOutline]}
                    onPress={handleSignUp}
                >
                    <Text style={styles.buttonRegisterText}>Register</Text>
                </TouchableOpacity>

                <View style={{ padding: 10 }}>
                    <Text style={{ fontWeight: 'bold' }}>Or</Text>
                </View>

                <TouchableOpacity
                    style={styles.buttonGuest}
                    onPress={() => navigation.navigate('Home')}
                >
                    <Text style={styles.buttonRegisterText}>
                        Continue as a guest
                    </Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
}
